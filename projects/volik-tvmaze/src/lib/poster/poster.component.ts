import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { VolikTvmazeService } from '../volik-tvmaze.service';

@Component({
  selector: 'vo-poster',
  template: `
    <img *ngIf="posterUrl$ | async as src" [src]="src" />
  `,
  styles: [
    `
      :host {
        display: inline-block;
        overflow: hidden;
        border-radius: 4px;
        line-height: 0;
      }
    `,
  ],
})
export class PosterComponent implements OnInit {
  @Input() showId: number;
  posterUrl$: Observable<string>;

  constructor(private tvmaze: VolikTvmazeService) {}

  ngOnInit() {
    this.posterUrl$ = this.tvmaze
      .getShow(this.showId)
      .pipe(map(show => show.image.medium));
  }
}
