import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VolikTvmazeComponent } from './volik-tvmaze.component';

describe('VolikTvmazeComponent', () => {
  let component: VolikTvmazeComponent;
  let fixture: ComponentFixture<VolikTvmazeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VolikTvmazeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VolikTvmazeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
