import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Show } from './volik-tvmaze.models';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class VolikTvmazeService {
  private readonly apiRoot = 'https://api.tvmaze.com';

  constructor(private http: HttpClient) {}

  getShow(id: number): Observable<Show> {
    const url = `${this.apiRoot}/shows/${id}`;
    return this.http.get<Show>(url);
  }
}
