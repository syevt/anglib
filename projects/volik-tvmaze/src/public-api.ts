/*
 * Public API Surface of volik-tvmaze
 */

export * from './lib/volik-tvmaze.service';
export * from './lib/volik-tvmaze.module';
export * from './lib/volik-tvmaze.models';
