import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Show, VolikTvmazeService } from 'projects/volik-tvmaze/src/public-api';

@Component({
  selector: 'ld-root',
  template: `
    <vo-poster [showId]="336"></vo-poster>
    <pre>{{ show$ | async | json }}</pre>
  `
})
export class AppComponent {
  show$: Observable<Show>;

  constructor(private tvmaze: VolikTvmazeService) {
    this.show$ = this.tvmaze.getShow(336);
  }
}
